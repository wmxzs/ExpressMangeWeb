package com.example.service;

import java.util.Date;

public interface AdminService {

    void updateLoginTime(String username, Date date, String ip);

    boolean login(String username, String password);
}
