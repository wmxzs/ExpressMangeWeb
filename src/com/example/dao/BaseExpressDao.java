package com.example.dao;

import com.example.bean.Express;
import com.example.exception.DuplicateCodeException;
import com.example.exception.DuplicateNumberException;

import java.util.List;
import java.util.Map;

public interface BaseExpressDao {

    List<Map<String,Integer>> console();

    List<Express> findAll(boolean limit, int... args);

    Express findByNumber(String number);

    Express findByCode(String code);

    List<Express> findByUserPhone(String userPhone);

    List<Express> findByUserPhoneAndStatus(String userPhone, int status);

    List<Express> findBySysPhone(String sysPhone);

    boolean insert(Express e) throws DuplicateCodeException, DuplicateNumberException;

    boolean update(int id,Express newExpress);

    boolean updateStatus(String code);

    boolean delete(int id);
}
