package com.example.exception;

public class DuplicateIdCardException extends Exception {
    public DuplicateIdCardException() {
    }

    public DuplicateIdCardException(String message) {
        super(message);
    }
}
