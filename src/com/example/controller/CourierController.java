package com.example.controller;

import com.example.bean.BootStrapTableCourier;
import com.example.bean.Courier;
import com.example.bean.Message;
import com.example.bean.ResultData;
import com.example.exception.DuplicateIdCardException;
import com.example.exception.DuplicatePhoneException;
import com.example.service.CourierService;
import com.example.service.impl.CourierServiceImpl;
import com.example.util.DateFormatUtil;
import com.example.util.JSONUtil;
import com.example.web.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CourierController {
    CourierService service = new CourierServiceImpl();

    @ResponseBody("/courier/console.do")
    public String console(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Integer> data = service.console();
        Message msg = new Message();
        if (data.size() == 0) {
            msg.setStatus(-1);
        } else {
            msg.setStatus(0);
        }
        msg.setData(data);
        return JSONUtil.toJSON(msg);
    }

    @ResponseBody("/courier/list.do")
    public String list(HttpServletRequest request, HttpServletResponse response) {
        //1. 获取分页查询数据的起始索引
        int offset = Integer.parseInt(request.getParameter("offset"));
        //2. 获取当前页要查询的数据量
        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
        //3. 进行查询
        List<Courier> list = service.findAll(true, offset, pageNumber);
        List<BootStrapTableCourier> list2 = new ArrayList<>();
        int i = 1;
        for (Courier courier : list) {
            String phone = courier.getPhone();
            list2.add(new BootStrapTableCourier(
                    i++,
                    courier.getName(),
                    courier.getPhone(),
                    courier.getIdCard(),
                    "*******",
                    courier.getNumberDispatch(),
                    courier.getStatus() == 0 ? "已激活" : "已删除",
                    DateFormatUtil.formatDate(courier.getSignUpTime()),
                    courier.getLoginTime() == null ? "未登录" : DateFormatUtil.formatDate(courier.getLoginTime())
            ));
        }
        Map<String, Integer> console = service.console();
        Integer size = console.get("size");
        //4. 将集合封装为 bootstrap-table识别的格式
        ResultData<BootStrapTableCourier> data = new ResultData<>();
        data.setRows(list2);
        data.setTotal(size);
        return JSONUtil.toJSON(data);
    }

    @ResponseBody("/courier/insert.do")
    public String insert(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idCard = request.getParameter("idCard");
        String password = request.getParameter("password");
        Message message = new Message();
        try {
            boolean insert = service.insert(new Courier(name, phone, idCard, password));
            if (insert) {
                message.setStatus(0);
                message.setContent("注册成功");
            }else {
                message.setStatus(-1);
                message.setContent("注册失败");
            }
        } catch (DuplicateIdCardException e) {
            message.setStatus(-1);
            message.setContent("身份证已被注册！");
        } catch (DuplicatePhoneException e) {
            message.setStatus(-1);
            message.setContent("手机号已被注册！");
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/courier/find.do")
    public String find(HttpServletRequest request, HttpServletResponse response) {
        String phone = request.getParameter("phone");
        Courier courier = service.findByPhone(phone);
        Message message = new Message();
        if (courier == null) {
            message.setStatus(-1);
            message.setContent("手机号码不存在");
        }else {
            message.setStatus(0);
            message.setContent("查询成功");
            message.setData(courier);
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/courier/update.do")
    public String update(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String idCard = request.getParameter("idCard");
        String password = request.getParameter("password");
        Message message = new Message();
        boolean update = false;
        try {
            update = service.update(id, new Courier(name, phone, idCard, password));
            if (update) {
                message.setStatus(0);
                message.setContent("修改成功");
            }else {
                message.setStatus(-1);
                message.setContent("修改失败");
            }
        } catch (DuplicatePhoneException e) {
            message.setStatus(-1);
            message.setContent("手机号已被注册！");
        } catch (DuplicateIdCardException e) {
            message.setStatus(-1);
            message.setContent("身份证已被注册！");
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/courier/delete.do")
    public String delete(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        String phone = request.getParameter("phone");
        String idCard = request.getParameter("idCard");
        Message message = new Message();
        boolean delete = service.delete(id, phone, idCard);
        if (delete) {
            message.setStatus(0);
            message.setContent("删除成功");
        }else {
            message.setStatus(-1);
            message.setContent("删除失败");
        }
        return JSONUtil.toJSON(message);
    }
}
