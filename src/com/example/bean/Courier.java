package com.example.bean;

import java.sql.Timestamp;
import java.util.Objects;

public class Courier {
    private int id;
    private String name;
    private String phone;
    private String idCard;
    private String password;
    private int numberDispatch;
    private int status;
    private Timestamp signUpTime;
    private Timestamp loginTime;

    public Courier() {
    }

    public Courier(String name, String phone, String idCard, String password) {
        this.name = name;
        this.phone = phone;
        this.idCard = idCard;
        this.password = password;
    }

    public Courier(int id, String name, String phone, String idCard, String password, int numberDispatch, int status, Timestamp signUpTime, Timestamp loginTime) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.idCard = idCard;
        this.password = password;
        this.numberDispatch = numberDispatch;
        this.status = status;
        this.signUpTime = signUpTime;
        this.loginTime = loginTime;
    }

    public Courier(String name, String password, String phone) {
        this.name = name;
        this.phone = phone;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNumberDispatch() {
        return numberDispatch;
    }

    public void setNumberDispatch(int numberDispatch) {
        this.numberDispatch = numberDispatch;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getSignUpTime() {
        return signUpTime;
    }

    public void setSignUpTime(Timestamp signUpTime) {
        this.signUpTime = signUpTime;
    }

    public Timestamp getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Timestamp loginTime) {
        this.loginTime = loginTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Courier courier = (Courier) o;
        return id == courier.id &&
                numberDispatch == courier.numberDispatch &&
                status == courier.status &&
                Objects.equals(name, courier.name) &&
                Objects.equals(phone, courier.phone) &&
                Objects.equals(idCard, courier.idCard) &&
                Objects.equals(password, courier.password) &&
                Objects.equals(signUpTime, courier.signUpTime) &&
                Objects.equals(loginTime, courier.loginTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, phone, idCard, password, numberDispatch, status, signUpTime, loginTime);
    }

    @Override
    public String toString() {
        return "Courier{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", idCard='" + idCard + '\'' +
                ", numberDispatch=" + numberDispatch +
                ", status=" + status +
                ", signUpTime=" + signUpTime +
                ", loginTime=" + loginTime +
                '}';
    }
}
