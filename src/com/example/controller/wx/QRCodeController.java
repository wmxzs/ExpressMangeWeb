package com.example.controller.wx;

import com.example.bean.Message;
import com.example.util.JSONUtil;
import com.example.util.UserUtil;
import com.example.web.ResponseBody;
import com.example.web.ResponseView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class QRCodeController {

    @ResponseView("/wx/createQRCode.do")
    public String createQRCode(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String type = request.getParameter("type");
        String code = null;
        String userPhone = null;
        String QRCodeContent = null;
        if ("express".equals(type)) {
            //快递二维码:被扫后，展示单个快递信息
            //code
            code = request.getParameter("code");
            QRCodeContent = "express_" + code;
        }else {
            //用户二维码:被扫后，快递员(柜子)端展示用户所有快递
            //userPhone
            userPhone = UserUtil.getUserPhone(session);
            QRCodeContent = "userPhone_" + userPhone;
        }
        session.setAttribute("qrcode", QRCodeContent);
        return "/wx/personQRcode.html";
    }

    @ResponseBody("/wx/qrcode.do")
    public String getQRCode(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String qrcode = (String) session.getAttribute("qrcode");
        Message message = new Message();
        if (qrcode == null) {
            message.setStatus(-1);
            message.setContent("取件码获取异常，请重新操作！");
        }else {
            message.setStatus(0);
            message.setContent(qrcode);
        }
        return JSONUtil.toJSON(message);
    }
}
