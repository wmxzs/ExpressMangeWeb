package com.example.dao;

import com.example.bean.User;
import com.example.exception.DuplicateIdCardException;
import com.example.exception.DuplicatePhoneException;

import java.util.List;
import java.util.Map;

public interface BaseUserDao {

    Map<String,Integer> console();

    List<User> findAll(boolean limit, int... args);

    void updateLoginTime(String userPhone);

    User findByPhone(String phone);

    boolean insert(User user) throws DuplicateIdCardException, DuplicatePhoneException;

    boolean signUp(String userPhone) throws DuplicatePhoneException;

    boolean update(int id, User newUser) throws DuplicateIdCardException, DuplicatePhoneException;

    boolean updateByWx(String userPhone, User newUser);

    boolean updateByPhone(String phone, User newUser) throws DuplicateIdCardException;

    boolean delete(int id, String phone, String idCard);
}
