package com.example.filter;

import com.example.util.UserUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/wx/*")
public class WxAccessControlFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String uri = request.getRequestURI();
        String userPhone = UserUtil.getUserPhone(request.getSession());
        if (userPhone != null || uri.endsWith("login.html") || uri.endsWith("500.html") ||
                uri.endsWith("login.do") || uri.endsWith("login2.do") || uri.endsWith("loginSms.do")||
                uri.endsWith(".js")) {
            filterChain.doFilter(servletRequest, servletResponse);
        }else {
            response.sendRedirect("/wx/login.html");
        }
    }

    @Override
    public void destroy() {

    }
}
