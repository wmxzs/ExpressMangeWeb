package com.example.controller;

import com.example.bean.BootStrapTableExpress;
import com.example.bean.Express;
import com.example.bean.Message;
import com.example.bean.ResultData;
import com.example.exception.DuplicateNumberException;
import com.example.service.ExpressService;
import com.example.service.impl.ExpressServiceImpl;
import com.example.util.DateFormatUtil;
import com.example.util.JSONUtil;
import com.example.web.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExpressController {

    ExpressService service = new ExpressServiceImpl();

    @ResponseBody("/express/console.do")
    public String console(HttpServletRequest request, HttpServletResponse response) {
        List<Map<String, Integer>> data = service.console();
        Message msg = new Message();
        if (data.size() == 0) {
            msg.setStatus(-1);
        } else {
            msg.setStatus(0);
        }
        msg.setData(data);
        return JSONUtil.toJSON(msg);
    }

    @ResponseBody("/express/list.do")
    public String list(HttpServletRequest request, HttpServletResponse response) {
        //1. 获取分页查询数据的起始索引
        int offset = Integer.parseInt(request.getParameter("offset"));
        //2. 获取当前页要查询的数据量
        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
        //3. 进行查询
        List<Express> list = service.findAll(true, offset, pageNumber);
        List<BootStrapTableExpress> list2 = new ArrayList<>();
        int i = 1;
        for (Express e : list) {
            String userPhone = e.getUserPhone();
            String showUserPhone = userPhone.substring(0, 3) + "****" + userPhone.substring(7, 11);
            String sysPhone = e.getSysPhone();
            String showSysPhone = sysPhone.substring(0, 3) + "****" + sysPhone.substring(7, 11);
            list2.add(new BootStrapTableExpress(
                    i++,
                    e.getNumber(),
                    e.getUsername(),
                    showUserPhone,
                    e.getCompany(),
                    e.getCode() == null ? "已取件" : e.getCode(),
                    DateFormatUtil.formatDate(e.getInTime()),
                    e.getOutTime() == null ? "未出库" : DateFormatUtil.formatDate(e.getOutTime()),
                    e.getStatus() == 0 ? "待取件" : "已取件",
                    showSysPhone
            ));
        }
        List<Map<String, Integer>> console = service.console();
        Integer total_express = console.get(0).get("total_express");
        //4. 将集合封装为 bootstrap-table识别的格式
        ResultData<BootStrapTableExpress> data = new ResultData<>();
        data.setRows(list2);
        data.setTotal(total_express);
        return JSONUtil.toJSON(data);
    }

    @ResponseBody("/express/insert.do")
    public String insert(HttpServletRequest request, HttpServletResponse response) {
        String number = request.getParameter("number");
        String company = request.getParameter("company");
        String username = request.getParameter("username");
        String userPhone = request.getParameter("userPhone");
        // 管理员界面添加快递，默认录入人手机号18888887888
//        boolean insert = service.insert(new Express(number, username, userPhone, company, UserUtil.getUserPhone(request.getSession())));
        boolean insert = false;
        Message message = null;
        try {
            insert = service.insert(new Express(number, username, userPhone, company, "18888887888"));
            if (insert) {
                //录入成功
                message = new Message(0, "录入成功");
            }else {
                //录入失败
                message = new Message(-1, "录入失败");
            }
        } catch (DuplicateNumberException e) {
            message = new Message(-1, "单号已存在");
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/express/find.do")
    public String find(HttpServletRequest request, HttpServletResponse response) {
        String number = request.getParameter("number");
        Express e = service.findByNumber(number);
        Message message = new Message();
        if (e == null) {
            message.setStatus(-1);
            message.setContent("单号不存在！");
        }else {
            message.setStatus(0);
            message.setContent("查询成功！");
            message.setData(e);
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/express/update.do")
    public String update(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        String number = request.getParameter("number");
        String company = request.getParameter("company");
        String username = request.getParameter("username");
        String userPhone = request.getParameter("userPhone");
        int status = Integer.parseInt(request.getParameter("status"));
        Express e = new Express();
        e.setNumber(number);
        e.setCompany(company);
        e.setUsername(username);
        e.setUserPhone(userPhone);
        e.setStatus(status);
        boolean update = false;
        Message message = new Message();
        try {
            update = service.update(id, e);
            if (update) {
                message.setStatus(0);
                message.setContent("修改成功");
            }else {
                message.setStatus(-1);
                message.setContent("修改失败");
            }
        } catch (DuplicateNumberException duplicateNumberException) {
            message.setStatus(-1);
            message.setContent("单号已存在!");
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/express/delete.do")
    public String delete(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        boolean delete = service.delete(id);
        Message message = new Message();
        if (delete) {
            message.setStatus(0);
            message.setContent("删除成功");
        }else {
            message.setStatus(-1);
            message.setContent("删除失败");
        }
        return JSONUtil.toJSON(message);
    }
}
