package com.example.controller.wx;

import com.example.util.wx.SignatureUtil;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/wx/wxconf.do")
public class WXConfig extends HttpServlet {

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		String urlText = request.getParameter("xurl");
		try {
			String json = SignatureUtil.getConfig(urlText).toJSON();
			pw.println(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		pw.close();
	}

}
