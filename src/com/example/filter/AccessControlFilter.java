package com.example.filter;

import com.example.util.UserUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter({"/admin/index.jsp","/admin/views/*","/express/*","/courier/*","/user/*"})
public class AccessControlFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String username = UserUtil.getAdminUserName(request.getSession());
        if (username != null) {
            filterChain.doFilter(servletRequest, servletResponse);
        }else {
            response.sendError(404, "你还未登录，请登录后操作！http://localhost:8080/admin/login.jsp");
        }
    }

    @Override
    public void destroy() {

    }
}
