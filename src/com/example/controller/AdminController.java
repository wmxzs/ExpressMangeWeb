package com.example.controller;

import com.example.bean.Message;
import com.example.service.AdminService;
import com.example.service.impl.AdminServiceImpl;
import com.example.util.JSONUtil;
import com.example.util.UserUtil;
import com.example.web.ResponseBody;
import com.example.web.ResponseView;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdminController {
    AdminService service = new AdminServiceImpl();

    @ResponseBody("/admin/login.do")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        ServletContext application = request.getServletContext();
        HttpSession session = request.getSession();
        List<String> loginList = (ArrayList<String>) application.getAttribute("loginList");
        if (loginList == null) {
            loginList = new ArrayList<>();
        }
        String username = request.getParameter("username");
        Message msg = null;
        if (loginList.contains(username)) {
            msg = new Message(-1, "该用户已登录，请勿重复登录！");
        }else {
            String password = request.getParameter("password");
            boolean login = service.login(username, password);
            if (login) {
                msg = new Message(0, "登录成功！");
                Date date = new Date();
                String ip = request.getRemoteAddr();
                service.updateLoginTime(username, date, ip);
                loginList.add(username);
                application.setAttribute("loginList", loginList);
                session.setAttribute("adminUsername", username);
                Cookie cookie = new Cookie("adminUsername", username);
                cookie.setMaxAge(60 * 60 * 24 * 7);
                response.addCookie(cookie);
            }else {
                msg = new Message(-1, "登录失败,用户名或密码错误！");
            }
        }
        return JSONUtil.toJSON(msg);
    }

    @ResponseBody("/admin/login2.do")
    public String login2(HttpServletRequest request, HttpServletResponse response) {
        ServletContext application = request.getServletContext();
        HttpSession session = request.getSession();
        List<String> loginList = (ArrayList<String>) application.getAttribute("loginList");
        if (loginList == null) {
            loginList = new ArrayList<>();
        }
        String username = null;
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie:cookies) {
            if (cookie.getName().equals("adminUsername")) {
                username = cookie.getValue();
            }
        }
        Message msg = new Message();
        if (loginList.contains(username)) {
            session.setAttribute("adminUsername", username);
            msg.setStatus(0);
        }else {
            msg.setStatus(-1);
        }
        return JSONUtil.toJSON(msg);
    }

    @ResponseView("/admin/quit.do")
    public String quit(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ServletContext application = request.getServletContext();
        ArrayList<String> loginList = (ArrayList<String>) application.getAttribute("loginList");
        String username = UserUtil.getAdminUserName(session);
        if (username != null) {
            session.invalidate();
            loginList.remove(username);
            application.setAttribute("loginList", loginList);
            Cookie cookie = new Cookie("adminUsername", username);
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        }
        return "login.html";
    }
}
