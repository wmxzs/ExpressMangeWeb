package com.example.web;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Documented
@Retention(RetentionPolicy.RUNTIME)

public @interface ResponseBody {
    String value();
}
