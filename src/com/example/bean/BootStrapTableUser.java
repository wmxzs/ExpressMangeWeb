package com.example.bean;

public class BootStrapTableUser {
    private int id;
    private String nickName;
    private String phone;
    private String idCard;
    private String password;
    private String status;
    private String signUpTime;
    private String loginTime;

    public BootStrapTableUser() {
    }

    public BootStrapTableUser(int id, String nickName, String phone, String idCard, String password, String status, String signUpTime, String loginTime) {
        this.id = id;
        this.nickName = nickName;
        this.phone = phone;
        this.idCard = idCard;
        this.password = password;
        this.status = status;
        this.signUpTime = signUpTime;
        this.loginTime = loginTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSignUpTime() {
        return signUpTime;
    }

    public void setSignUpTime(String signUpTime) {
        this.signUpTime = signUpTime;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }
}
