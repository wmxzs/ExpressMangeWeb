package com.example.exception;

public class DuplicatePhoneException extends Exception{
    public DuplicatePhoneException() {
    }

    public DuplicatePhoneException(String message) {
        super(message);
    }
}
