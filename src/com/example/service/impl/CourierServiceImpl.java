package com.example.service.impl;

import com.example.bean.Courier;
import com.example.dao.BaseCourierDao;
import com.example.dao.impl.CourierDaoMysql;
import com.example.exception.DuplicateIdCardException;
import com.example.exception.DuplicatePhoneException;
import com.example.service.CourierService;

import java.util.List;
import java.util.Map;

public class CourierServiceImpl implements CourierService {

    BaseCourierDao dao = new CourierDaoMysql();

    @Override
    public Map<String, Integer> console() {
        return dao.console();
    }

    @Override
    public List<Courier> findAll(boolean limit, int... args) {
        return dao.findAll(limit, args);
    }

    @Override
    public void updateLoginTime(String userPhone) {
        dao.updateLoginTime(userPhone);
    }

    @Override
    public Courier findByPhone(String phone) {
        return dao.findByPhone(phone);
    }

    @Override
    public boolean insert(Courier courier) throws DuplicateIdCardException, DuplicatePhoneException {
        return dao.insert(courier);
    }

    @Override
    public boolean numberDispatchAddOne(String userPhone) {
        return dao.numberDispatchAddOne(userPhone);
    }

    @Override
    public boolean update(int id, Courier newCourier) throws DuplicatePhoneException, DuplicateIdCardException {
        return dao.update(id, newCourier);
    }

    @Override
    public boolean updateByWx(String userPhone, Courier newCourier) {
        return dao.updateByWx(userPhone, newCourier);
    }

    @Override
    public boolean delete(int id, String phone, String idCard) {
        return dao.delete(id, phone, idCard);
    }
}
