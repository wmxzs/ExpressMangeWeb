package com.example.exception;

public class DuplicateNumberException extends Exception {

    public DuplicateNumberException() {
    }

    public DuplicateNumberException(String message) {
        super(message);
    }
}
