package com.example.dao.impl;

import com.example.bean.User;
import com.example.dao.BaseUserDao;
import com.example.exception.DuplicateIdCardException;
import com.example.exception.DuplicatePhoneException;
import com.example.util.DruidUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class UserDaoMysql extends DruidUtil implements BaseUserDao {

    private static final String SQL_CONSOLE = "SELECT " +
            "COUNT(id) as size, " +
            "COUNT(TO_DAYS(signUpTime) = TO_DAYS(NOW()) OR NULL) as daySize " +
            "FROM user";

    private static final String SQL_FIND_ALL = "SELECT * FROM user";

    private static final String SQL_FIND_LIMIT = "SELECT * FROM user LIMIT ?,?";

    private static final String SQL_UPDATE_LOGINTIME = "UPDATE user SET loginTime=NOW() WHERE phone=?";

    private static final String SQL_FIND_BY_PHONE = "SELECT * FROM user WHERE phone=?";

    private static final String SQL_INSERT = "INSERT INTO user (nickName,phone,idCard,password,status,signUpTime) VALUES (?,?,?,?,2,NOW())";

    private static final String SQL_SIGNUP = "INSERT INTO user (phone,signUpTime) VALUES (?, NOW())";

    private static final String SQL_UPDATE = "UPDATE user SET nickName=?,phone=?,idCard=?,password=?,status=2,signUpTime=Now() WHERE id=?";

    private static final String SQL_UPDATE_BY_WX = "UPDATE user SET nickName=?,password=? WHERE phone=?";

    private static final String SQL_UPDATE_BY_PHONE = "UPDATE user SET nickName=?,idCard=?,password=?,status=2 WHERE phone=?";

    private static final String SQL_DELETE = "UPDATE user SET phone=?,idCard=?,status=1 WHERE id=?";


    @Override
    public Map<String, Integer> console() {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        Map<String, Integer> data = new HashMap<>();
        try {
            statement = connection.prepareStatement(SQL_CONSOLE);
            res = statement.executeQuery();
            while (res.next()) {
                int size = res.getInt("size");
                int daySize = res.getInt("daySize");
                data.put("size", size);
                data.put("daySize", daySize);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            close(connection, statement, res);
        }
        return data;
    }

    @Override
    public List<User> findAll(boolean limit, int... args) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        List<User> list = new ArrayList<>();
        try {
            if (limit) {
                statement = connection.prepareStatement(SQL_FIND_LIMIT);
                statement.setInt(1, args[0]);
                statement.setInt(2, args[1]);
            }else {
                statement = connection.prepareStatement(SQL_FIND_ALL);
            }
            res = statement.executeQuery();
            while (res.next()) {
                list.add(new User(
                        res.getInt("id"),
                        res.getString("nickName"),
                        res.getString("phone"),
                        res.getString("idCard"),
                        res.getString("password"),
                        res.getInt("status"),
                        res.getTimestamp("signUpTime"),
                        res.getTimestamp("loginTime")
                ));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(connection, statement, res);
        }
        return list;
    }

    @Override
    public void updateLoginTime(String userPhone) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE_LOGINTIME);
            statement.setString(1, userPhone);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(connection, statement, res);
        }
    }

    @Override
    public User findByPhone(String phone) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        User user = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_PHONE);
            statement.setString(1, phone);
            res = statement.executeQuery();
            while (res.next()) {
                user = new User(
                        res.getInt("id"),
                        res.getString("nickName"),
                        res.getString("phone"),
                        res.getString("idCard"),
                        res.getString("password"),
                        res.getInt("status"),
                        res.getTimestamp("signUpTime"),
                        res.getTimestamp("loginTime")
                );
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(connection, statement, res);
        }
        return user;
    }



    @Override
    public boolean insert(User user) throws DuplicateIdCardException, DuplicatePhoneException {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        try {
            statement = connection.prepareStatement(SQL_INSERT);
            statement.setString(1, user.getNickName());
            statement.setString(2, user.getPhone());
            statement.setString(3, user.getIdCard());
            statement.setString(4, user.getPassword());
            return statement.executeUpdate() > 0;
        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            if (throwables.getMessage().endsWith("for key 'idCard'")) {
                throw new DuplicateIdCardException(throwables.getMessage());
            }else if (throwables.getMessage().endsWith("for key 'phone'")) {
                throw new DuplicatePhoneException(throwables.getMessage());
            }else {
                throwables.printStackTrace();
            }
        } finally {
            close(connection, statement, res);
        }
        return false;
    }

    @Override
    public boolean signUp(String userPhone) throws DuplicatePhoneException {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        try {
            statement = connection.prepareStatement(SQL_SIGNUP);
            statement.setString(1, userPhone);
            return statement.executeUpdate() > 0;
        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            if (throwables.getMessage().endsWith("for key 'phone'")) {
                throw new DuplicatePhoneException(throwables.getMessage());
            }else {
                throwables.printStackTrace();
            }
        } finally {
            close(connection, statement, res);
        }
        return false;
    }

    @Override
    public boolean update(int id, User newUser) throws DuplicateIdCardException, DuplicatePhoneException {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, newUser.getNickName());
            statement.setString(2, newUser.getPhone());
            statement.setString(3, newUser.getIdCard());
            statement.setString(4, newUser.getPassword());
            statement.setInt(5, id);
            return statement.executeUpdate() > 0;
        } catch (SQLException throwables) {
            if (throwables.getMessage().endsWith("for key 'idCard'")) {
                throw new DuplicateIdCardException(throwables.getMessage());
            }else if (throwables.getMessage().endsWith("for key 'phone'")) {
                throw new DuplicatePhoneException(throwables.getMessage());
            }else {
                throwables.printStackTrace();
            }
        } finally {
            close(connection, statement, res);
        }
        return false;
    }

    @Override
    public boolean updateByWx(String userPhone, User newUser) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE_BY_WX);
            statement.setString(1, newUser.getNickName());
            statement.setString(2, newUser.getPassword());
            statement.setString(3, newUser.getPhone());
            return statement.executeUpdate() > 0;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(connection, statement, res);
        }
        return false;
    }

    @Override
    public boolean updateByPhone(String phone, User newUser) throws DuplicateIdCardException {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE_BY_PHONE);
            statement.setString(1, newUser.getNickName());
            statement.setString(2, newUser.getIdCard());
            statement.setString(3, newUser.getPassword());
            statement.setString(4, phone);
            return statement.executeUpdate() > 0;
        } catch (SQLException throwables) {
            if (throwables.getMessage().endsWith("for key 'idCard'")) {
                throw new DuplicateIdCardException(throwables.getMessage());
            }else {
                throwables.printStackTrace();
            }
        } finally {
            close(connection, statement, res);
        }
        return false;
    }

    @Override
    public boolean delete(int id, String phone, String idCard) {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        ResultSet res = null;
        try {
            phone += "#";
            idCard += "#";
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setString(1, phone);
            statement.setString(2, idCard);
            statement.setInt(3, id);
            return statement.executeUpdate() > 0;
        } catch (SQLException throwables) {

        } finally {
            close(connection, statement, res);
        }
        return false;
    }
}
