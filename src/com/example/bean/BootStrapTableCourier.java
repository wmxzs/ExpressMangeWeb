package com.example.bean;

public class BootStrapTableCourier {
    private int id;
    private String name;
    private String phone;
    private String idCard;
    private String password;
    private int numberDispatch;
    private String status;
    private String signUpTime;
    private String loginTime;

    public BootStrapTableCourier() {
    }

    public BootStrapTableCourier(int id, String name, String phone, String idCard, String password, int numberDispatch, String status, String signUpTime, String loginTime) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.idCard = idCard;
        this.password = password;
        this.numberDispatch = numberDispatch;
        this.status = status;
        this.signUpTime = signUpTime;
        this.loginTime = loginTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNumberDispatch() {
        return numberDispatch;
    }

    public void setNumberDispatch(int numberDispatch) {
        this.numberDispatch = numberDispatch;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSignUpTime() {
        return signUpTime;
    }

    public void setSignUpTime(String signUpTime) {
        this.signUpTime = signUpTime;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }
}
