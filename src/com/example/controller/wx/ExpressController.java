package com.example.controller.wx;

import com.example.bean.BootStrapTableExpress;
import com.example.bean.Express;
import com.example.bean.Message;
import com.example.exception.DuplicateNumberException;
import com.example.service.CourierService;
import com.example.service.ExpressService;
import com.example.service.impl.CourierServiceImpl;
import com.example.service.impl.ExpressServiceImpl;
import com.example.util.DateFormatUtil;
import com.example.util.JSONUtil;
import com.example.util.UserUtil;
import com.example.web.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class ExpressController {

    ExpressService service = new ExpressServiceImpl();

    @ResponseBody("/wx/findExpressByUserPhone.do")
    public String findExpressByUserPhone(HttpServletRequest request, HttpServletResponse response) {
        String userPhone = UserUtil.getUserPhone(request.getSession());
        List<Express> expressList = service.findByUserPhone(userPhone);
        List<BootStrapTableExpress> list2 = new ArrayList<>();
        Message message = new Message();
        if (expressList.size() == 0) {
            message.setStatus(-1);
        }else {
            message.setStatus(0);
            for (Express e : expressList) {
                list2.add(new BootStrapTableExpress(
                        e.getId(),
                        e.getNumber(),
                        e.getUsername(),
                        userPhone,
                        e.getCompany(),
                        e.getCode() == null ? "已取件" : e.getCode(),
                        DateFormatUtil.formatDate(e.getInTime()),
                        e.getOutTime() == null ? "未出库" : DateFormatUtil.formatDate(e.getOutTime()),
                        e.getStatus() == 0 ? "待取件" : "已取件",
                        e.getSysPhone()
                ));
            }
            Stream<BootStrapTableExpress> status0Express = list2.stream().filter(express -> {
                if (express.getStatus().equals("待取件")) {
                    return true;
                } else {
                    return false;
                }
            }).sorted(((o1, o2) -> {
                long time1 = DateFormatUtil.toTime(o1.getInTime());
                long time2 = DateFormatUtil.toTime(o2.getInTime());
                return (int) (time1 - time2);
            }));
            Stream<BootStrapTableExpress> status1Express = list2.stream().filter(express -> {
                if (express.getStatus().equals("已取件")) {
                    return true;
                } else {
                    return false;
                }
            }).sorted(((o1, o2) -> {
                long time1 = DateFormatUtil.toTime(o1.getOutTime());
                long time2 = DateFormatUtil.toTime(o2.getOutTime());
                return (int) (time1 - time2);
            }));
            Object[] s0 = status0Express.toArray();
            Object[] s1 = status1Express.toArray();
            Map<String, Object> data = new HashMap<>();
            data.put("status0", s0);
            data.put("status1", s1);
            message.setData(data);
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/wx/list.do")
    public String list(HttpServletRequest request, HttpServletResponse response) {
        List<Express> list = service.findBySysPhone(UserUtil.getUserPhone(request.getSession()));
        List<BootStrapTableExpress> list2 = new ArrayList<>();
        Message message = new Message();
        if (list.size() == 0) {
            message.setStatus(-1);
            message.setContent("查询为空");
        }else {
            int i = 1;
            for (Express e : list) {
                String userPhone = e.getUserPhone();
                String showUserPhone = userPhone.substring(0, 3) + "****" + userPhone.substring(7, 11);
                list2.add(new BootStrapTableExpress(
                        i++,
                        e.getNumber(),
                        e.getUsername(),
                        showUserPhone,
                        e.getCompany(),
                        e.getCode() == null ? "已取件" : "******",
                        DateFormatUtil.formatDate(e.getInTime()),
                        e.getOutTime() == null ? "未出库" : DateFormatUtil.formatDate(e.getOutTime()),
                        e.getStatus() == 0 ? "待取件" : "已取件",
                        e.getSysPhone()
                ));
            }
            message.setStatus(0);
            message.setContent("查询成功");
            message.setData(list2);
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/wx/userExpressList.do")
    public String userExpressList(HttpServletRequest request, HttpServletResponse response) {
        String userPhone = request.getParameter("userPhone");
        List<Express> expressList = service.findByUserPhoneAndStatus(userPhone, 0);
        List<BootStrapTableExpress> list2 = new ArrayList<>();
        Message message = new Message();
        if (expressList.size() == 0) {
            message.setStatus(-1);
            message.setContent("未查询到快递！");
        }else {
            for (Express e : expressList) {
                list2.add(new BootStrapTableExpress(
                        e.getId(),
                        e.getNumber(),
                        e.getUsername(),
                        userPhone,
                        e.getCompany(),
                        e.getCode() == null ? "已取件" : e.getCode(),
                        DateFormatUtil.formatDate(e.getInTime()),
                        e.getOutTime() == null ? "未出库" : DateFormatUtil.formatDate(e.getOutTime()),
                        e.getStatus() == 0 ? "待取件" : "已取件",
                        e.getSysPhone()
                ));
            }
            Stream<BootStrapTableExpress> status0Express = list2.stream().sorted(((o1, o2) -> {
                long time1 = DateFormatUtil.toTime(o1.getInTime());
                long time2 = DateFormatUtil.toTime(o2.getInTime());
                return (int) (time1 - time2);
            }));
            Object[] s0 = status0Express.toArray();
            message.setStatus(0);
            message.setContent("查询成功！");
            message.setData(s0);
        }

        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/wx/findByCode.do")
    public String findByCode(HttpServletRequest request, HttpServletResponse response) {
        String code = request.getParameter("code");
        Express e = service.findByCode(code);
        Message message = new Message();
        if (e == null) {
            message.setStatus(-1);
            message.setContent("取件码不存在！");
        }else {
            BootStrapTableExpress e2 = new BootStrapTableExpress(
                    e.getId(),
                    e.getNumber(),
                    e.getUsername(),
                    e.getUserPhone(),
                    e.getCompany(),
                    e.getCode(),
                    DateFormatUtil.formatDate(e.getInTime()),
                    e.getOutTime() == null ? "未出库":DateFormatUtil.formatDate(e.getOutTime()),
                    e.getStatus() == 0 ? "未取件":"已取件",
                    e.getSysPhone()
            );
            message.setStatus(0);
            message.setContent("查询成功");
            message.setData(e2);
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/wx/findByNumber.do")
    public String findByNumber(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String number = request.getParameter("number");
        Express e = service.findByNumber(number);
        Message message = new Message();
        if (e == null) {
            message.setStatus(-1);
            message.setContent("单号不存在！");
        }else {
            BootStrapTableExpress e2 = new BootStrapTableExpress(
                    e.getId(),
                    e.getNumber(),
                    e.getUsername(),
                    e.getUserPhone(),
                    e.getCompany(),
                    e.getCode(),
                    DateFormatUtil.formatDate(e.getInTime()),
                    e.getOutTime() == null ? "未出库":DateFormatUtil.formatDate(e.getOutTime()),
                    e.getStatus() == 0 ? "未取件":"已取件",
                    e.getSysPhone()
            );
            if ("未取件".equals(e2.getStatus())) {
                message.setStatus(0);
            }else {
                message.setStatus(1);
            }
            message.setContent("查询成功");
            message.setData(e2);
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/wx/updateStatus.do")
    public String updateStatus(HttpServletRequest request, HttpServletResponse response) {
        String code = request.getParameter("code");
        boolean flag = service.updateStatus(code);
        Message message = new Message();
        if (flag) {
            message.setStatus(0);
            message.setContent("取件成功！");
        }else {
            message.setStatus(-1);
            message.setContent("取件失败！");
        }
        return JSONUtil.toJSON(message);
    }

    @ResponseBody("/wx/insert.do")
    public String insert(HttpServletRequest request, HttpServletResponse response) {
        String number = request.getParameter("number");
        String company = request.getParameter("company");
        String username = request.getParameter("username");
        String userPhone = request.getParameter("userPhone");
        boolean insert = false;
        Message message = null;
        try {
            insert = service.insert(new Express(number, username, userPhone, company, UserUtil.getUserPhone(request.getSession())));
            if (insert) {
                //录入成功
                message = new Message(0, "录入成功");
                CourierService service = new CourierServiceImpl();
                service.numberDispatchAddOne(userPhone);
            }else {
                //录入失败
                message = new Message(-1, "录入失败");
            }
        } catch (DuplicateNumberException e) {
            message = new Message(-1, "单号已存在！");
        }
        return JSONUtil.toJSON(message);
    }
}
