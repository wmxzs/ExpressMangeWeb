package com.example.bean;

import java.sql.Timestamp;
import java.util.Objects;

public class User {
    private int id;
    private String nickName;
    private String phone;
    private String idCard;
    private String password;
    private int status;
    private Timestamp signUpTime;
    private Timestamp loginTime;

    public User() {
    }

    public User(int id, String nickName, String phone, String idCard, String password, int status, Timestamp signUpTime, Timestamp loginTime) {
        this.id = id;
        this.nickName = nickName;
        this.phone = phone;
        this.idCard = idCard;
        this.password = password;
        this.status = status;
        this.signUpTime = signUpTime;
        this.loginTime = loginTime;
    }

    public User(String nickName, String phone, String idCard, String password) {
        this.nickName = nickName;
        this.phone = phone;
        this.idCard = idCard;
        this.password = password;
    }

    public User(String nickName, String idCard, String password) {
        this.nickName = nickName;
        this.idCard = idCard;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getSignUpTime() {
        return signUpTime;
    }

    public void setSignUpTime(Timestamp signUpTime) {
        this.signUpTime = signUpTime;
    }

    public Timestamp getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Timestamp loginTime) {
        this.loginTime = loginTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                status == user.status &&
                Objects.equals(nickName, user.nickName) &&
                Objects.equals(phone, user.phone) &&
                Objects.equals(idCard, user.idCard) &&
                Objects.equals(password, user.password) &&
                Objects.equals(signUpTime, user.signUpTime) &&
                Objects.equals(loginTime, user.loginTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nickName, phone, idCard, password, status, signUpTime, loginTime);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", nickName='" + nickName + '\'' +
                ", phone='" + phone + '\'' +
                ", idCard='" + idCard + '\'' +
                ", status=" + status +
                ", signUpTime=" + signUpTime +
                ", loginTime=" + loginTime +
                '}';
    }
}
