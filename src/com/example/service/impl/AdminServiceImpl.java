package com.example.service.impl;

import com.example.dao.BaseAdminDao;
import com.example.dao.impl.AdminDaoMysql;
import com.example.service.AdminService;

import java.util.Date;

public class AdminServiceImpl implements AdminService {

    private static BaseAdminDao dao = new AdminDaoMysql();

    @Override
    public void updateLoginTime(String username, Date date, String ip) {
        dao.updateLoginTime(username, date, ip);
    }

    @Override
    public boolean login(String username, String password) {
        return dao.login(username, password);
    }
}
