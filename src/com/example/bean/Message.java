package com.example.bean;

public class Message {

    private int status;

    private String content;

    private Object data;

    public Message() {
    }

    public Message(int status) {
        this.status = status;
    }

    public Message(int status, String content, Object data) {
        this.status = status;
        this.content = content;
        this.data = data;
    }

    public Message(int status, String content) {
        this.status = status;
        this.content = content;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
