package com.example.dao;

import com.example.bean.Courier;
import com.example.exception.DuplicateIdCardException;
import com.example.exception.DuplicatePhoneException;

import java.util.List;
import java.util.Map;

public interface BaseCourierDao {

    Map<String,Integer> console();

    List<Courier> findAll(boolean limit, int... args);

    void updateLoginTime(String userPhone);

    Courier findByPhone(String phone);

    boolean insert(Courier courier) throws DuplicateIdCardException, DuplicatePhoneException;

    boolean numberDispatchAddOne(String userPhone);

    boolean update(int id, Courier newCourier) throws DuplicateIdCardException, DuplicatePhoneException;

    boolean updateByWx(String userPhone, Courier newCourier);

    boolean delete(int id, String phone, String idCard);
}
