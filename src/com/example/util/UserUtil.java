package com.example.util;

import com.example.bean.User;

import javax.servlet.http.HttpSession;

public class UserUtil {
    public static String getAdminUserName(HttpSession session) {
        return (String) session.getAttribute("adminUsername");
    }

    public static String getUserName(HttpSession session) {
        return (String) session.getAttribute("username");
    }

    public static String getUserPhone(HttpSession session) {
        return (String) session.getAttribute("userPhone");
    }

    public static String getLoginSms(HttpSession session, String userPhone) {
        return (String) session.getAttribute(userPhone);
    }
    public static void setWxUser(HttpSession session, User user){
        session.setAttribute("wxUser",user);
    }

    public static User getWxUser(HttpSession session){
        return (User) session.getAttribute("wxUser");
    }
    public static void setLoginSms(HttpSession session, String userPhone, String code) {
        session.setAttribute(userPhone, code);
    }

    public static void setUserInfo(HttpSession session, int index) {
        session.setAttribute("userInfo", index);
    }

    public static int getUserInfo(HttpSession session) {
        return (int) session.getAttribute("userInfo");
    }
}
