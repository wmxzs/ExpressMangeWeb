package com.example.dao;

import java.util.Date;


public interface BaseAdminDao {

    void updateLoginTime(String username, Date date, String ip);

    boolean login(String username, String password);
}
