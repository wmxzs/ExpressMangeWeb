package com.example.service.impl;

import com.example.bean.User;
import com.example.dao.BaseUserDao;
import com.example.dao.impl.UserDaoMysql;
import com.example.exception.DuplicateIdCardException;
import com.example.exception.DuplicatePhoneException;
import com.example.service.UserService;

import java.util.List;
import java.util.Map;

public class UserServiceImpl implements UserService {

    BaseUserDao dao = new UserDaoMysql();

    @Override
    public Map<String, Integer> console() {
        return dao.console();
    }

    @Override
    public List<User> findAll(boolean limit, int... args) {
        return dao.findAll(limit, args);
    }

    @Override
    public void updateLoginTime(String userPhone) {
        dao.updateLoginTime(userPhone);
    }

    @Override
    public User findByPhone(String phone) {
        return dao.findByPhone(phone);
    }

    @Override
    public boolean insert(User user) throws DuplicateIdCardException, DuplicatePhoneException {
        return dao.insert(user);
    }

    @Override
    public boolean signUp(String userPhone) throws DuplicatePhoneException {
        return dao.signUp(userPhone);
    }

    @Override
    public boolean update(int id, User newUser) throws DuplicatePhoneException, DuplicateIdCardException {
        return dao.update(id, newUser);
    }

    @Override
    public boolean updateByWx(String userPhone, User newUser) {
        return dao.updateByWx(userPhone, newUser);
    }

    @Override
    public boolean updateByPhone(String phone, User newUser) throws DuplicateIdCardException {
        return dao.updateByPhone(phone, newUser);
    }

    @Override
    public boolean delete(int id, String phone, String idCard) {
        return dao.delete(id, phone, idCard);
    }
}
